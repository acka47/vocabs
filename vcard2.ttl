@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix : <http://www.w3.org/2006/vcard/ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://www.w3.org/2006/vcard/ns>
    a owl:Ontology ;
    rdfs:comment "Ontology for vCard based on RFC6350"^^xsd:string ;
    rdfs:label "Ontology for vCard"^^xsd:string ;
    owl:versionInfo "Draft"^^xsd:string .

:Acquaintance
    a owl:Class ;
    rdfs:label "Acquaintance"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Address
    a owl:Class ;
    rdfs:comment "To specify the components of the delivery address for the  object"^^xsd:string ;
    rdfs:label "Address"^^xsd:string ;
    owl:equivalentClass [
        a owl:Class ;
        owl:unionOf ([
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :country-name ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty :country-name
                    ]
                )
            ]
            [
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :locality ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty :locality
                    ]
                )
            ]
            [
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :postal-code ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty :postal-code
                    ]
                )
            ]
            [
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :region ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty :region
                    ]
                )
            ]
            [
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :street-address ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty :street-address
                    ]
                )
            ]
        )
    ] .

:Agent
    a owl:Class ;
    rdfs:label "Agent"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:BBS
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "BBS"@en ;
    rdfs:subClassOf :TelephoneType ;
    owl:deprecated true .

:Car
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Car"@en ;
    rdfs:subClassOf :TelephoneType ;
    owl:deprecated true .

:Cell
    a owl:Class ;
    rdfs:comment "Also called mobile telephone"^^xsd:string ;
    rdfs:label "Cell"^^xsd:string ;
    rdfs:subClassOf :TelephoneType .

:Child
    a owl:Class ;
    rdfs:label "Child"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Colleague
    a owl:Class ;
    rdfs:label "Colleague"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Contact
    a owl:Class ;
    rdfs:label "Contact"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Coresident
    a owl:Class ;
    rdfs:label "Coresident"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Coworker
    a owl:Class ;
    rdfs:label "Coworker"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Crush
    a owl:Class ;
    rdfs:label "Crush"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Date
    a owl:Class ;
    rdfs:label "Date"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Dom
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Dom"@en ;
    rdfs:subClassOf :Type ;
    owl:deprecated true .

:Email
    a owl:Class ;
    rdfs:comment "To specify the electronic mail address for communication with the object the vCard represents. Use the hasEmail object property."^^xsd:string ;
    rdfs:label "Email"^^xsd:string ;
    owl:deprecated true .

:Emergency
    a owl:Class ;
    rdfs:label "Emergency"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Fax
    a owl:Class ;
    rdfs:label "Fax"^^xsd:string ;
    rdfs:subClassOf :TelephoneType .

:Female
    a owl:Class ;
    rdfs:label "Female"^^xsd:string ;
    rdfs:subClassOf :Gender .

:Friend
    a owl:Class ;
    rdfs:label "Friend"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Gender
    a owl:Class ;
    rdfs:comment "Used for gender codes. The URI of the gender code must be used as the value for Gender." ;
    rdfs:label "Gender"^^xsd:string .

:Group
    a owl:Class ;
    rdfs:comment "Object representing a group of persons or entities.  A group object will usually contain hasMember properties to specify the members of the group."^^xsd:string ;
    rdfs:label "Group"^^xsd:string ;
    rdfs:subClassOf :Kind ;
    owl:disjointWith :Individual, :Location, :Organization ;
    owl:equivalentClass [
        a owl:Class ;
        owl:intersectionOf ([
                a owl:Restriction ;
                owl:onProperty :hasMember ;
                owl:someValuesFrom :Kind
            ]
            [
                a owl:Restriction ;
                owl:minQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
                owl:onClass :Kind ;
                owl:onProperty :hasMember
            ]
        )
    ] .

:Home
    a owl:Class ;
    rdfs:comment "This implies that the property is related to an individual's personal life"^^xsd:string ;
    rdfs:label "Home"^^xsd:string ;
    rdfs:subClassOf :Type .

:ISDN
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "ISDN"@en ;
    rdfs:subClassOf :Type ;
    owl:deprecated true .

:Individual
    a owl:Class ;
    rdfs:comment "An object representing a single person or entity"^^xsd:string ;
    rdfs:label "Individual"^^xsd:string ;
    rdfs:subClassOf :Kind ;
    owl:disjointWith :Location, :Organization .

:Internet
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Internet"@en ;
    rdfs:subClassOf :Type ;
    owl:deprecated true .

:Intl
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Intl"@en ;
    rdfs:subClassOf :Type ;
    owl:deprecated true .

:Kin
    a owl:Class ;
    rdfs:label "Kin"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Kind
    a owl:Class ;
    rdfs:comment "The parent class for all objects"^^xsd:string ;
    rdfs:label "Kind"^^xsd:string ;
    owl:equivalentClass :VCard, [
        a owl:Restriction ;
        owl:minQualifiedCardinality "1"^^xsd:nonNegativeInteger ;
        owl:onDataRange xsd:string ;
        owl:onProperty :fn
    ] .

:Label
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Label"@en ;
    rdfs:subClassOf :Type ;
    owl:deprecated true .

:Location
    a owl:Class ;
    rdfs:comment "An object representing a named geographical place"^^xsd:string ;
    rdfs:label "Location"^^xsd:string ;
    rdfs:subClassOf :Kind ;
    owl:disjointWith :Organization .

:Male
    a owl:Class ;
    rdfs:label "Male"^^xsd:string ;
    rdfs:subClassOf :Gender .

:Me
    a owl:Class ;
    rdfs:label "Me"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Met
    a owl:Class ;
    rdfs:label "Met"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Modem
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Modem"@en ;
    rdfs:subClassOf :TelephoneType ;
    owl:deprecated true .

:Msg
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Msg"@en ;
    rdfs:subClassOf :TelephoneType ;
    owl:deprecated true .

:Muse
    a owl:Class ;
    rdfs:label "Muse"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Name
    a owl:Class ;
    rdfs:comment "To specify the components of the name of the object"^^xsd:string ;
    rdfs:label "Name"^^xsd:string ;
    owl:equivalentClass [
        a owl:Class ;
        owl:unionOf ([
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :additional-name ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty :additional-name
                    ]
                )
            ]
            [
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :family-name ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty :family-name
                    ]
                )
            ]
            [
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :given-name ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:maxCardinality "1"^^xsd:nonNegativeInteger ;
                        owl:onProperty :given-name
                    ]
                )
            ]
            [
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :honorific-prefix ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty :honorific-prefix
                    ]
                )
            ]
            [
                a owl:Class ;
                owl:intersectionOf ([
                        a owl:Restriction ;
                        owl:onProperty :honorific-suffix ;
                        owl:someValuesFrom xsd:string
                    ]
                    [
                        a owl:Restriction ;
                        owl:minCardinality "0"^^xsd:nonNegativeInteger ;
                        owl:onProperty :honorific-suffix
                    ]
                )
            ]
        )
    ] .

:Neighbor
    a owl:Class ;
    rdfs:label "Neighbor"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:None
    a owl:Class ;
    rdfs:label "None"^^xsd:string ;
    rdfs:subClassOf :Gender .

:Organization
    a owl:Class ;
    rdfs:comment """An object representing an organization.  An organization is a single entity, and might represent a business or government, a department or division within a business or government, a club, an association, or the like.
"""^^xsd:string ;
    rdfs:label "Organization"^^xsd:string ;
    rdfs:subClassOf :Kind .

:Other
    a owl:Class ;
    rdfs:label "Other"^^xsd:string ;
    rdfs:subClassOf :Gender .

:PCS
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "PCS"@en ;
    rdfs:subClassOf :TelephoneType ;
    owl:deprecated true .

:Pager
    a owl:Class ;
    rdfs:label "Pager"^^xsd:string ;
    rdfs:subClassOf :TelephoneType .

:Parcel
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Parcel"@en ;
    rdfs:subClassOf :Type ;
    owl:deprecated true .

:Parent
    a owl:Class ;
    rdfs:label "Parent"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Postal
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Postal"@en ;
    rdfs:subClassOf :Type ;
    owl:deprecated true .

:Pref
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "Pref"@en ;
    rdfs:subClassOf :Type ;
    owl:deprecated true .

:RelatedType
    a owl:Class ;
    rdfs:comment "Used for relation type codes. The URI of the relation type code must be used as the value for the Relation Type." ;
    rdfs:label "Relation Type"^^xsd:string .

:Sibling
    a owl:Class ;
    rdfs:label "Sibling"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Spouse
    a owl:Class ;
    rdfs:label "Spouse"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Sweetheart
    a owl:Class ;
    rdfs:label "Sweetheart"^^xsd:string ;
    rdfs:subClassOf :RelatedType .

:Tel
    a owl:Class ;
    rdfs:comment "This class is deprecated. Use the hasTelephone object property." ;
    rdfs:label "Tel"@en ;
    owl:deprecated true .

:TelephoneType
    a owl:Class ;
    rdfs:comment "Used for telephone type codes. The URI of the telephone type code must be used as the value for the Telephone Type." ;
    rdfs:label "Phone"^^xsd:string .

:Text
    a owl:Class ;
    rdfs:comment "Also called sms telephone"^^xsd:string ;
    rdfs:label "Text"^^xsd:string ;
    rdfs:subClassOf :TelephoneType .

:TextPhone
    a owl:Class ;
    rdfs:label "Text phone"^^xsd:string ;
    rdfs:subClassOf :TelephoneType .

:Type
    a owl:Class ;
    rdfs:comment "Used for type codes. The URI of the type code must be used as the value for Type." ;
    rdfs:label "Type"^^xsd:string .

:Unknown
    a owl:Class ;
    rdfs:label "Unknown"^^xsd:string ;
    rdfs:subClassOf :Gender .

:VCard
    a owl:Class ;
    rdfs:comment "The vCard class is deprecated and equivalent to the new Kind class, which is the parent for the four explicit types of vCards (Individual, Organization, Location, Group)" ;
    rdfs:label "VCard"@en ;
    owl:deprecated true .

:Video
    a owl:Class ;
    rdfs:label "Video"^^xsd:string ;
    rdfs:subClassOf :TelephoneType .

:Voice
    a owl:Class ;
    rdfs:label "Voice"^^xsd:string ;
    rdfs:subClassOf :TelephoneType .

:Work
    a owl:Class ;
    rdfs:comment "This implies that the property is related to an individual's work place"^^xsd:string ;
    rdfs:label "Work"^^xsd:string ;
    rdfs:subClassOf :Type .

:X400
    a owl:Class ;
    rdfs:comment "This class is deprecated" ;
    rdfs:label "X400"@en ;
    rdfs:subClassOf :Type ;
    owl:deprecated true .

:additional-name
    a owl:DatatypeProperty ;
    rdfs:comment "The additional name associated with the object" ;
    rdfs:label "additional name"^^xsd:string ;
    rdfs:range xsd:string .

:adr
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated" ;
    rdfs:label "address"@en ;
    owl:deprecated true ;
    owl:equivalentProperty :hasAddress .

:agent
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated" ;
    rdfs:label "agent"@en ;
    owl:deprecated true .

:anniversary
    a owl:DatatypeProperty ;
    rdfs:comment "The date of marriage, or equivalent, of the object"^^xsd:string ;
    rdfs:label "anniversary"^^xsd:string ;
    rdfs:range xsd:dateTime .

:bday
    a owl:DatatypeProperty ;
    rdfs:comment "To specify the birth date of the object"^^xsd:string ;
    rdfs:label "birth date"^^xsd:string ;
    rdfs:range xsd:dateTime .

:category
    a owl:DatatypeProperty ;
    rdfs:comment "The category information about the object, also known as tags" ;
    rdfs:label "category"^^xsd:string ;
    rdfs:range xsd:string .

:class
    a owl:DatatypeProperty ;
    rdfs:comment "This data property has been deprecated" ;
    rdfs:label "class"@en ;
    owl:deprecated true .

:country-name
    a owl:DatatypeProperty ;
    rdfs:comment "The country name associated with the address of the object" ;
    rdfs:label "country name"^^xsd:string ;
    rdfs:range xsd:string .

:email
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated"^^xsd:string ;
    rdfs:label "email"^^xsd:string ;
    owl:deprecated true ;
    owl:equivalentProperty :hasEmail .

:extended-address
    a owl:DatatypeProperty ;
    rdfs:comment "This data property has been deprecated" ;
    rdfs:label "extended address"@en ;
    owl:deprecated true .

:family-name
    a owl:DatatypeProperty ;
    rdfs:comment "The family name associated with the object" ;
    rdfs:label "family name"^^xsd:string ;
    rdfs:range xsd:string .

:fn
    a owl:DatatypeProperty ;
    rdfs:comment "The formatted text corresponding to the name of the object" ;
    rdfs:label "formatted name"^^xsd:string ;
    rdfs:range xsd:string .

:geo
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated"^^xsd:string ;
    rdfs:label "geo"^^xsd:string ;
    owl:deprecated true ;
    owl:equivalentProperty :hasGeo .

:given-name
    a owl:DatatypeProperty ;
    rdfs:comment "The given name associated with the object" ;
    rdfs:label "given name"^^xsd:string ;
    rdfs:range xsd:string .

:hasAdditionalName
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the additional name data property"^^xsd:string ;
    rdfs:label "has additional name"@en .

:hasAddress
    a owl:ObjectProperty ;
    rdfs:comment "To specify the components of the delivery address for the object"^^xsd:string ;
    rdfs:label "has address"^^xsd:string ;
    rdfs:range :Address .

:hasCalendarBusy
    a owl:ObjectProperty ;
    rdfs:comment "To specify the busy time associated with the object. (Was called FBURL in RFC6350)"^^xsd:string ;
    rdfs:label "has calendar busy"^^xsd:string .

:hasCalendarLink
    a owl:ObjectProperty ;
    rdfs:comment "To specify the calendar associated with the object. (Was called CALURI in RFC6350)"^^xsd:string ;
    rdfs:label "has calendar link"^^xsd:string .

:hasCalendarRequest
    a owl:ObjectProperty ;
    rdfs:comment "To specify the calendar user address to which a scheduling request be sent for the object. (Was called CALADRURI in RFC6350)"^^xsd:string ;
    rdfs:label "has calendar request"^^xsd:string .

:hasCategory
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the category data property"^^xsd:string ;
    rdfs:label "has category"@en .

:hasCountryName
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the country name data property"^^xsd:string ;
    rdfs:label "has country name"^^xsd:string .

:hasEmail
    a owl:ObjectProperty ;
    rdfs:comment "To specify the electronic mail address for communication with the object"^^xsd:string ;
    rdfs:label "has email"^^xsd:string ;
    rdfs:range :Email .

:hasFN
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the formatted name data property"^^xsd:string ;
    rdfs:label "has formatted name"@en .

:hasFamilyName
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the family name data property"^^xsd:string ;
    rdfs:label "has family name"@en .

:hasGender
    a owl:ObjectProperty ;
    rdfs:comment """To specify  the sex or gender identity of the object.
URIs are recommended to enable interoperable sex and gender codes to be used."""^^xsd:string ;
    rdfs:label "has gender"@en .

:hasGeo
    a owl:ObjectProperty ;
    rdfs:comment "To specify information related to the global positioning of the object. May also be used as a property parameter."^^xsd:string ;
    rdfs:label "has geo"^^xsd:string .

:hasGivenName
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the given name data property"^^xsd:string ;
    rdfs:label "has given name"@en .

:hasHonorificPrefix
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the honorific prefix data property"^^xsd:string ;
    rdfs:label "has honorific prefix"@en .

:hasHonorificSuffix
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the honorific suffix data property"^^xsd:string ;
    rdfs:label "has honorific suffix"@en .

:hasInstantMessage
    a owl:ObjectProperty ;
    rdfs:comment "To specify the instant messaging and presence protocol communications with the object. (Was called IMPP in RFC6350)"^^xsd:string ;
    rdfs:label "has messaging"^^xsd:string .

:hasKey
    a owl:ObjectProperty ;
    rdfs:comment "To specify a public key or authentication certificate associated with the object"^^xsd:string ;
    rdfs:label "has key"^^xsd:string ;
    owl:equivalentProperty :key .

:hasLanguage
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the language data property"^^xsd:string ;
    rdfs:label "has language"@en .

:hasLocality
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the locality data property"^^xsd:string ;
    rdfs:label "has locality"@en .

:hasLogo
    a owl:ObjectProperty ;
    rdfs:comment "To specify a graphic image of a logo associated with the object "^^xsd:string ;
    rdfs:label "has logo"^^xsd:string ;
    owl:equivalentProperty :logo .

:hasMember
    a owl:ObjectProperty ;
    rdfs:comment "To include a member in the group this object represents. (This property can only be used by Group individuals)"^^xsd:string ;
    rdfs:domain :Group ;
    rdfs:label "has member"^^xsd:string ;
    rdfs:range :Kind .

:hasName
    a owl:ObjectProperty ;
    rdfs:comment "To specify the components of the name of the object"^^xsd:string ;
    rdfs:label "has name"^^xsd:string ;
    rdfs:range :Name ;
    owl:equivalentProperty :n .

:hasNickname
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the nickname data property"^^xsd:string ;
    rdfs:label "has nickname"@en ;
    rdfs:seeAlso " http://www.w3.org/2006/vcard/ns#nickname"^^xsd:anyURI .

:hasNote
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the note data property"^^xsd:string ;
    rdfs:label "has note"@en .

:hasOrganizationName
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the organization name data property"^^xsd:string ;
    rdfs:label "has organization name"@en .

:hasOrganizationUnit
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the organization unit name data property"^^xsd:string ;
    rdfs:label "has organization unit name"@en .

:hasPhoto
    a owl:ObjectProperty ;
    rdfs:comment "To specify an image or photograph information that annotates some aspect of the object"^^xsd:string ;
    rdfs:label "has photo"^^xsd:string ;
    owl:equivalentProperty :photo .

:hasPostalCode
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the postal code data property"^^xsd:string ;
    rdfs:label "has postal code"@en .

:hasRegion
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the region data property"^^xsd:string ;
    rdfs:label "has region"@en .

:hasRelated
    a owl:ObjectProperty ;
    rdfs:comment "To specify a relationship between another entity and the entity represented by this object"^^xsd:string ;
    rdfs:label "has related"^^xsd:string .

:hasRole
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the role data property"^^xsd:string ;
    rdfs:label "has role"@en .

:hasSound
    a owl:ObjectProperty ;
    rdfs:comment "To specify a digital sound content information that annotates some aspect of the object"^^xsd:string ;
    rdfs:label "has sound"^^xsd:string ;
    owl:equivalentProperty :sound .

:hasSource
    a owl:ObjectProperty ;
    rdfs:comment "To identify the source of directory information of the object" ;
    rdfs:label "has source"@en .

:hasStreetAddress
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the street address data property"^^xsd:string ;
    rdfs:label "has street address"@en .

:hasTelephone
    a owl:ObjectProperty ;
    rdfs:comment "To specify the telephone number for telephony communication with the object"^^xsd:string ;
    rdfs:label "has telephone"^^xsd:string ;
    owl:equivalentProperty :tel .

:hasTitle
    a owl:ObjectProperty ;
    rdfs:comment "Used to support property parameters for the title data property"^^xsd:string ;
    rdfs:label "has title"@en .

:hasUID
    a owl:ObjectProperty ;
    rdfs:comment "To specify a value that represents a globally unique identifier corresponding to the object"^^xsd:string ;
    rdfs:label "has uid"@en .

:hasURL
    a owl:ObjectProperty ;
    rdfs:comment "To specify a uniform resource locator associated with the object"^^xsd:string ;
    rdfs:label "has url"^^xsd:string ;
    owl:equivalentProperty :url .

:hasValue
    a owl:ObjectProperty ;
    rdfs:comment "Used to indicate the resource value of an object property that requires property parameters"^^xsd:string ;
    rdfs:label "has value"@en .

:honorific-prefix
    a owl:DatatypeProperty ;
    rdfs:comment "The honorific prefix of the name associated with the object" ;
    rdfs:label "honorific prefix"^^xsd:string ;
    rdfs:range xsd:string .

:honorific-suffix
    a owl:DatatypeProperty ;
    rdfs:comment "The honorific suffix of the name associated with the object" ;
    rdfs:label "honorific suffix"^^xsd:string ;
    rdfs:range xsd:string .

:key
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated"^^xsd:string ;
    rdfs:label "key"@en ;
    owl:deprecated true .

:label
    a owl:DatatypeProperty ;
    rdfs:comment "This data property has been deprecated" ;
    rdfs:label "label"@en ;
    owl:deprecated true .

:language
    a owl:DatatypeProperty ;
    rdfs:comment "To specify the language that may be used for contacting the object. May also be used as a property parameter." ;
    rdfs:label "language"@en .

:latitude
    a owl:DatatypeProperty ;
    rdfs:comment "This data property has been deprecated" ;
    rdfs:label "latitude"@en ;
    owl:deprecated true .

:locality
    a owl:DatatypeProperty ;
    rdfs:comment "The locality (e.g. city or town) associated with the address of the object" ;
    rdfs:label "locality"^^xsd:string ;
    rdfs:range xsd:string .

:logo
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated"^^xsd:string ;
    rdfs:label "logo"@en ;
    owl:deprecated true .

:longitude
    a owl:DatatypeProperty ;
    rdfs:comment "This data property has been deprecated" ;
    rdfs:label "longitude"@en ;
    owl:deprecated true .

:mailer
    a owl:DatatypeProperty ;
    rdfs:comment "This data property has been deprecated" ;
    rdfs:label "mailer"@en ;
    owl:deprecated true .

:n
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated" ;
    rdfs:label "name"@en ;
    owl:deprecated true .

:nickname
    a owl:DatatypeProperty ;
    rdfs:comment "The nick name associated with the object" ;
    rdfs:label "nickname"^^xsd:string ;
    rdfs:range xsd:string .

:note
    a owl:DatatypeProperty ;
    rdfs:comment "A note associated with the object" ;
    rdfs:label "note"^^xsd:string ;
    rdfs:range xsd:string .

:org
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated. Use the organization-name data property." ;
    rdfs:label "organization"@en ;
    owl:deprecated true .

:organization-name
    a owl:DatatypeProperty ;
    rdfs:comment "To specify the organizational name associated with the object" ;
    rdfs:label "organization name"^^xsd:string ;
    rdfs:range xsd:string .

:organization-unit
    a owl:DatatypeProperty ;
    rdfs:comment "To specify the organizational unit name associated with the object" ;
    rdfs:label "organizational unit name"^^xsd:string ;
    rdfs:range xsd:string ;
    rdfs:subPropertyOf :organization-name .

:photo
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated"^^xsd:string ;
    rdfs:label "photo"@en ;
    owl:deprecated true .

:post-office-box
    a owl:DatatypeProperty ;
    rdfs:comment "This data property has been deprecated" ;
    rdfs:label "post office box"@en ;
    owl:deprecated true .

:postal-code
    a owl:DatatypeProperty ;
    rdfs:comment "The postal code associated with the address of the object" ;
    rdfs:label "postal code"^^xsd:string ;
    rdfs:range xsd:string .

:prodid
    a owl:DatatypeProperty ;
    rdfs:comment "To specify the identifier for the product that created the object" ;
    rdfs:label "product id"^^xsd:string ;
    rdfs:range xsd:string .

:region
    a owl:DatatypeProperty ;
    rdfs:comment "The region (e.g. state or province) associated with the address of the object" ;
    rdfs:label "region"^^xsd:string ;
    rdfs:range xsd:string .

:rev
    a owl:DatatypeProperty ;
    rdfs:comment "To specify revision information about the object" ;
    rdfs:label "revision"^^xsd:string ;
    rdfs:range xsd:dateTime .

:role
    a owl:DatatypeProperty ;
    rdfs:comment "To specify the function or part played in a particular situation by the object" ;
    rdfs:label "role"^^xsd:string ;
    rdfs:range xsd:string .

:sort-string
    a owl:DatatypeProperty ;
    rdfs:comment "To specify the string to be used for national-language-specific sorting. Used as a property parameter only." ;
    rdfs:label "sort as"^^xsd:string ;
    rdfs:range xsd:string .

:sound
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated"^^xsd:string ;
    rdfs:label "sound"@en ;
    owl:deprecated true .

:street-address
    a owl:DatatypeProperty ;
    rdfs:comment "The street address associated with the address of the object" ;
    rdfs:label "street address"^^xsd:string ;
    rdfs:range xsd:string .

:tel
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated" ;
    rdfs:label "telephone"@en ;
    owl:deprecated true .

:title
    a owl:DatatypeProperty ;
    rdfs:comment "To specify the position or job of the object" ;
    rdfs:label "title"^^xsd:string ;
    rdfs:range xsd:string .

:tz
    a owl:DatatypeProperty ;
    rdfs:comment "To indicate time zone information that is specific to the object. May also be used as a property parameter." ;
    rdfs:label "time zone"^^xsd:string ;
    rdfs:range xsd:string .

:url
    a owl:ObjectProperty ;
    rdfs:comment "This object property has been deprecated"^^xsd:string ;
    rdfs:label "url"@en ;
    owl:deprecated true .

:value
    a owl:DatatypeProperty ;
    rdfs:comment "Used to indicate the literal value of a data property that requires property parameters"^^xsd:string ;
    rdfs:label "value"@en .

